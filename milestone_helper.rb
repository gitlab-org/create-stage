class MilestoneHelper
    class << self
        def milestones
            @milestones ||= [
                { start_date: monday_before(third_thursday(2024, 9)), name: "17.5" },
                { start_date: monday_before(third_thursday(2024, 10)), name: "17.6" },
                { start_date: monday_before(third_thursday(2024, 11)), name: "17.7" },
                { start_date: monday_before(third_thursday(2024, 12)), name: "17.8" },
                { start_date: monday_before(third_thursday(2025, 1)), name: "17.9" },
                { start_date: monday_before(third_thursday(2025, 2)), name: "17.10" },
                { start_date: monday_before(third_thursday(2025, 3)), name: "17.11" },
                { start_date: monday_before(third_thursday(2025, 4)), name: "18.0" },
                { start_date: monday_before(third_thursday(2025, 5)), name: "18.1" },
                { start_date: monday_before(third_thursday(2025, 6)), name: "18.2" },
                { start_date: monday_before(third_thursday(2025, 7)), name: "18.3" },
                { start_date: monday_before(third_thursday(2025, 8)), name: "18.4" },
                { start_date: monday_before(third_thursday(2025, 9)), name: "18.5" },
                { start_date: monday_before(third_thursday(2025, 10)), name: "18.6" },
                { start_date: monday_before(third_thursday(2025, 11)), name: "18.7" },
                { start_date: monday_before(third_thursday(2025, 12)), name: "18.8" },
                { start_date: monday_before(third_thursday(2026, 1)), name: "18.9" },
                { start_date: monday_before(third_thursday(2026, 2)), name: "18.10" },
                { start_date: monday_before(third_thursday(2026, 3)), name: "18.11" }
            ]
            @milestones
        end

        def previous_milestone(root_day = Date.today)
            previous = milestones.reverse.find { |milestone| milestone[:start_date] <= root_day }

            previous ? previous[:name] : "X.X"
        end

        def upcoming_milestone(root_day = Date.today)
            upcoming = milestones.find { |milestone| milestone[:start_date] > root_day }

            upcoming ? upcoming[:name] : "X.X"
        end

        # calculate the date of the third thursday of a given month
        def third_thursday(year, month)
            date = Date.new(year, month, 1)
            date += 1 until date.thursday?
            date += 14
            date
        end

        # calculate the monday before a given date
        def monday_before(date)
            date -= 1 until date.monday?
            date
        end
    end
end
