@gitlab-com/gitlab-ux/create-ux It's time for a new milestone kickoff! :tada:

### Purpose

> As we don't have a UX kickoff, where each designer shortly talks about an issue they are excited to be working on, we could just record ourself doing so and share that with the other Create designers.

Doing these short, async, monthly design kick-off videos will ideally help our team to:

1. Discovery what others are working
1. Identify any overlapping work
1. Help surface opportunities for where group work should overlap
1. Make it easier to consume the information regardless of our timezone differences
1. Encourage the practice of sharing ones work

### Format

* Let's make the videos no longer than 5 mins.
* Upload your video to [GitLab unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A), set visibility to "Public" (unless your video contains [confidential information](https://about.gitlab.com/handbook/communication/#not-public)) and add it to the [Create UX playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrUvA91eFQedd6zrvH0_kGY).

### Template

```markdown
### Title/group name or label

Video: 

References:
1. 
1. 
```
