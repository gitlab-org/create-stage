Release planning for the [Source Code Group](https://about.gitlab.com/handbook/product/categories/#source-code-group)

## Overview

### Key Themes

| Category | Theme |
| ------ | ------ |
|   |   |

## Team Capacity

- **Total Weight Capacity:** TODO
- **FE Weight:** TODO
- **BE Weight:** TODO

### Planning

- [Planning Board - For the entire group](https://gitlab.com/groups/gitlab-org/-/boards/7577682?label_name%5B%5D=group%3A%3Asource%20code&milestone_title=<MILESTONE>)
- [Planning Board - SCM Backlog](https://gitlab.com/groups/gitlab-org/-/boards/7657028?label_name%5B%5D=group%3A%3Asource%20code&label_name%5B%5D=scm-backlog) (new)
- [Planning Board - Per milestone](https://gitlab.com/groups/gitlab-org/-/boards/7658776?label_name%5B%5D=group%3A%3Asource%20code) (new)
- [UX Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/5092292)
- **Backend**
  - [Planning Board - Backend](https://gitlab.com/groups/gitlab-org/-/boards/7577683?label_name%5B%5D=backend&label_name%5B%5D=group%3A%3Asource%20code&milestone_title=<MILESTONE>)
  - [Next 1-3 Milestones - Backend](https://gitlab.com/groups/gitlab-org/-/boards/7153926?milestone_title=Next%201-3%20releases&label_name%5B%5D=group%3A%3Asource%20code&label_name%5B%5D=backend)
  - [SCM Backlog - Backend](https://gitlab.com/groups/gitlab-org/-/boards/7657169?label_name%5B%5D=backend&label_name%5B%5D=group%3A%3Asource%20code) (new)
  - [SCM Backend - Per milestone](https://gitlab.com/groups/gitlab-org/-/boards/7658776?label_name%5B%5D=group::source%20code&label_name%5B%5D=backend)
- **Frontend**
  - [Planning Board - Frontend](https://gitlab.com/groups/gitlab-org/-/boards/7577686?label_name%5B%5D=frontend&label_name%5B%5D=group%3A%3Asource%20code&milestone_title=<MILESTONE>)
  - [Next 1-3 Milestones - Frontend](https://gitlab.com/groups/gitlab-org/-/boards/7153926?label_name[]=group%3A%3Asource%20code&label_name[]=frontend&milestone_title=Next%201-3%20releases)
  - [SCM Backlog - Frontend](https://gitlab.com/groups/gitlab-org/-/boards/7657028?label_name%5B%5D=group%3A%3Asource%20code&label_name%5B%5D=scm-backlog&label_name%5B%5D=frontend) (new)
  - [SCM Frontend - Per milestone](https://gitlab.com/groups/gitlab-org/-/boards/7658776?label_name%5B%5D=group::source%20code&label_name%5B%5D=frontend)


### Build Boards

- [UX Build Board](https://gitlab.com/groups/gitlab-org/-/boards/5092276)
- [Backend Build Board](https://gitlab.com/groups/gitlab-org/-/boards/7577683?label_name%5B%5D=backend&label_name%5B%5D=Deliverable&label_name%5B%5D=group%3A%3Asource%20code&milestone_title=<MILESTONE>)
- [Frontend Build Board](https://gitlab.com/groups/gitlab-org/-/boards/7577686?label_name%5B%5D=frontend&label_name%5B%5D=Deliverable&label_name%5B%5D=group%3A%3Asource%20code&milestone_title=<MILESTONE>)

Product manager prepares the planning with proposed issues for the milestone. Engineering reviews and make recommendations based engineering allocation and capacity.


### Checklist

**Frontend Engineering Manager**

- [ ] Get Team Capacities and post them in a comment no later than the 10th of the month
- [ ] Propose prioritized list of type::maintenance issues in a comment
- [ ] Obtain weights for proposed issues
- [ ] Collaborate with product manager to finalize the plan
- [ ] Assign ~Deliverable Items for Milestone
- [ ] Add an issue comment if a deliverable should be highlighted in the release post


**Backend Engineering Manager**

- [ ] Get Team Capacities and post them in a comment no later than the 10th of the month
- [ ] Propose prioritized list of type::maintenance issues in a comment
- [ ] Obtain weights for proposed issues
- [ ] Collaborate with product manager to finalize the plan
- [ ] Assign ~Deliverable Items for Milestone
- [ ] Add an issue comment if a deliverable should be highlighted in the release post

**Quality Engineer**
- [ ] Propose prioritized list of type::bug issues in a comment and request weights from engineering where still necessary


**Product Manager**

- [ ] Define Key Themes
- [ ] Propose prioritized list of type::feature issues in a comment and request weights from engineering where still necessary
- [ ] Collaborate with counterparts in engineering to finalize the plan
- [ ] Add an issue comment if a deliverable should be highlighted in the release post
- [ ] Update the Direction page
- [ ] Sign off on final plan for the milestone
- [ ] Close the plannning issue


**Stable counterparts**

- [ ] Technical Writing (@brendan777)
- [ ] Quality (@jay_mccure)

([improve this template?](https://gitlab.com/gitlab-org/create-stage/-/blob/master/.gitlab/issue_templates/source-code-planning.md))

/label ~"group::source code" ~"type::ignore"
/assign @andr3 @dpoosarla @mcbabin
/milestone %<MILESTONE>



