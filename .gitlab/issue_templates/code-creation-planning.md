Release planning for the [Code Creation Group](https://handbook.gitlab.com/handbook/product/categories/features/#create-code-creation-group)

## Overview

- Start Date: YYYY-MM-DD
- End Date: YYYY-MM-DD
- Release Date: YYYY-MM-DD

## Issue Boards

- [Code Creation Workflow Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/5998095)
- [Code Creation Milestone Priority Board](https://gitlab.com/groups/gitlab-org/-/boards/8947676)
- [Code Creation Release Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/7316344) 
- [Code Creation Refinement Board](https://gitlab.com/groups/gitlab-org/-/boards/8994789)

## Planning Tasks

- [ ] Set the correct dates above. You can use [this tool](https://mnohr.gitlab.io/milestone-dates/) to lookup the dates.
- [ ] Refine the issues already assigned to the upcoming milestone for correct label hygiene
- [ ] Check/update past milestones for wayward issues

## Main Goals

_Add the main goals for this milestone here_

## Issue Priorities

For issues below:

- 🔺 higher priority
- no triangle: to follow higher priority
- 🔻 lower priority

## Product Themes

### Theme 1

- _Issue 1_
- _Issue 2_

## Additional Issues

### Bugs

- _Issue 1_
- _Issue 2_

### Maintenance

- _Issue 1_
- _Issue 2_

## Priority Issues

High Priority

```glql
display: table
fields: assignee, labels("code-creation-priority::*", "type::*", "Deliverable", "workflow::*"), weight, title
query: group = "gitlab-org" and milestone = "<MILESTONE>" and label = "group::code creation" and label = "code-creation-priority::high" and label != "type::ignore"
```

Issues needing refinement

```glql
display: table
fields: assignee, labels("code-creation-priority::*", "type::*", "Deliverable", "workflow::*"), weight, title
query: group = "gitlab-org" and milestone = "<MILESTONE>" and label = "group::code creation" and label = "workflow::refinement" and label != "type::ignore"
```

-----

_[Improve this Template](https://gitlab.com/gitlab-org/create-stage/-/blob/master/.gitlab/issue_templates/code-creation-planning.md)_

/label ~"group::code creation" ~"devops::create" ~"section::dev" ~"Planning Issue" ~"type::ignore"
/assign @mnohr @jordanjanes 
/milestone %<MILESTONE>