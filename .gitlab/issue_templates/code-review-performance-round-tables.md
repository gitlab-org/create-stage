<!--
Ensure the issue is titled: 🟤 Code Review Performance Round Table: MONTH DAY, YEAR
-->
# What is this?

This is an issue created to foster discussions leading up to the next sync round table session where we will carry sync discussions about the topics highlighted in this issue.

# What do I need to do?

Please share in the comments of this issue any ideas you'd like to share and discuss with the entire group for potential performance improvements.


# Goal

This issue and sessions are targeted at everyone in the Code Review group: Backend engineers, Frontend engineers, Product Manager, Designers.

We look to identify potential performance work that will bring performance improvements through these discussions. 

Some ideas:
* **incremental innovations:** small steps we can take to improve performance of Merge Requests.
* **radical innovations:** large projects that bring technological innovations to improve performance of Merge Requests.
* **open questions:** have we tried X or Y? Maybe you heard someone mention this new approach and you think it might apply to making Merge Requests faster
* **problems that need solving:** it's important that we keep our eyes on the users' perspective and what problems need solving. identifying them properly is half the victory.

# Questions?

Feel free to ask in #g_create_code-review

/label ~"Performance Round Tables::Code Review" 
/due in 1 week
