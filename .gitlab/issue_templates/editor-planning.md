Release planning for the [Editor Group](https://about.gitlab.com/handbook/product/categories/#editor-group)

## Overview

### [<MILESTONE> Release Board](https://gitlab.com/groups/gitlab-org/-/boards/1131977?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&milestone_title=<MILESTONE>) | [<MILESTONE>: Deliverable / Stretch Board](https://gitlab.com/groups/gitlab-org/-/boards/364216?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&milestone_title=<MILESTONE>)


### Key Themes

| Category | Theme |
| ------ | ------ |
|   |   |

### UX

Please see the [UX Board](https://gitlab.com/groups/gitlab-org/-/boards/1124143?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Deliverable&label_name[]=UX&label_name[]=group%3A%3Aeditor&milestone_title=<MILESTONE>) for this release

## Team Capacity

- **Total Weight Capacity:** TODO
- **FE Weight:** TODO
- **BE Weight:** TODO

### Frontend

[Frontend <MILESTONE>: Deliverable / Stretch Board](https://gitlab.com/groups/gitlab-org/-/boards/364216?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&label_name[]=frontend&milestone_title=<MILESTONE>)

Please see the [FE Board](https://gitlab.com/groups/gitlab-org/-/boards/1124142?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Deliverable&label_name[]=frontend&label_name[]=group%3A%3Aeditor&milestone_title=<MILESTONE>) for this release


### Backend

[Backend <MILESTONE>: Deliverable / Stretch Board](https://gitlab.com/groups/gitlab-org/-/boards/364216?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&label_name[]=backend&milestone_title=<MILESTONE>)

Please see the [BE Board](https://gitlab.com/groups/gitlab-org/-/boards/1124134?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Deliverable&label_name[]=backend&label_name[]=group%3A%3Aeditor&milestone_title=<MILESTONE>) for this release

### Checklist

**EM @oregand**

- [ ] Get Team Capacity
- [ ] Assign ~Direction Items for Milestone

**PM @ericschurter**

- [ ] Define Key Themes

### Stable counterparts

- [ ] Technical Writing (@aqualls)

([improve this template?](https://gitlab.com/gitlab-org/create-stage/-/blob/master/.gitlab/issue_templates/editor-planning.md))

/label ~"group::editor"
/assign @oregand @ericschurter @mle @dmishunov @fjsanpedro
/milestone %<MILESTONE>
