Milestone planning for the [Static Site Editor group](https://about.gitlab.com/handbook/product/categories/#static-site-editor-group)

Milestone dates: 2020-xx-23 to 2020-xx-22  
Velocity from previous milestone: `?` 

## Deliverables

| Issue | Category | Weight | Owner | 
| ------ | ------ | ------ | ------ |
| Issue name and link | ~"Category:Static Site Editor" or ~"Category:GitLab Handbook" | 1,2,3 or 5 | Team member name |

## Stretch

| Issue | Category | Weight | 
| ------ | ------ | ------ |
| Issue name and link | ~"Category:Static Site Editor" or ~"Category:GitLab Handbook" | 1,2,3 or 5 | Team member name |


## Planning tasks
- [ ] PM: Confirm deliverables for milestones based on roadmap prioritization
- [ ] EM: Add security or bug fix issues
- [ ] EM: Confirm available capacity for milestone based on PTO and holidays
- [ ] EM: Ensure issues are groomed and weights assigned
- [ ] EM: Confirm planned issues match the available capacity and work with PM to trim issues if needed
- [ ] PM: Confirm final list of planned issues matches customer expectation
